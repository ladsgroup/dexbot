import pywikibot
import json
import time
from handle_item import handle_item
site = pywikibot.Site('wikidata', 'wikidata')
try:
    with open('cases.json', 'r') as f:
        cases = json.loads(f.read())
except:
    print('not ready')
    sys.exit()
new_cases = {}
for item_title in cases:
    seconds_delta = time.time() - cases[item_title]['notification']
    if seconds_delta < (72*3600):
        new_cases[item_title] = cases[item_title]
        continue
    item = pywikibot.ItemPage(site, item_title)
    try:
        item.get()
    except:
        continue
    user = handle_item(item)
    if not user:
        if seconds_delta < (720*3600):
            new_cases[item_title] = cases[item_title]
        continue
    item.delete('Bot: Automatic deletion of empty item')
    with open('cases.json', 'w') as f:
        f.write(json.dumps(new_cases))

