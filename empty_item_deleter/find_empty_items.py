import pywikibot
from pywikibot.flow import Board
import json
import time
from handle_item import handle_item

from pywikibot.pagegenerators import WikidataSPARQLPageGenerator
query = r"""SELECT ?item WHERE {
  ?item wikibase:statements 0 .
  ?item wikibase:sitelinks ?s.
  FILTER(?s = 0).
} LIMIT 100
"""
topic_title = 'Automated report of empty item: {title}'
message = """Hello, [[{title}|an item]] that you have edited (and you are the only non-bot editor) is considered empty and will be deleted in 72 hours if it doesn't improve. Your automated cleaner"""
site = pywikibot.Site('wikidata', 'wikidata')
gen = WikidataSPARQLPageGenerator(query, site=site.data_repository(),
                                  endpoint='https://query.wikidata.org/sparql')
try:
    with open('cases.json', 'r') as f:
        cases = json.loads(f.read())
except:
    cases = {}
    with open('cases.json', 'w') as f:
        f.write(json.dumps(cases))

for item in gen:
    if item.title() in cases:
        print('In the system')
        continue
    user = handle_item(item)
    if not user:
        continue
    user_talk_page = pywikibot.Page(site, 'User talk:' + user)
    try:
        talk_page_text = user_talk_page.get()
    except:
        continue
    talk_page_text += '\n==' + topic_title.format(title=item.title()) + '==\n' + message.format(title=item.title()) + ', ~~~~'
    try:
        user_talk_page.put(talk_page_text, 'Bot: Automated notification of imminent deletion')
    except:
        flowpage = Board(user_talk_page)
        flowpage.new_topic(topic_title.format(title=item.title()), message.format(title=item.title()) + '.')
    cases[item.title()] = {'notification': time.time()}
    with open('cases.json', 'w') as f:
        f.write(json.dumps(cases))

