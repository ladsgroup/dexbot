import sys
from users_lib import get_hoomans
from datetime import datetime, timedelta
def handle_item(item):
    try:
        item.get()
    except KeyboardInterrupt:
        sys.exit()
    except:
        return False

    if len(item.sitelinks.values()) or len(item.claims):
        return False
    ref_gen = list(item.getReferences(namespaces=0))
    if ref_gen:
        return False
    
    users = set()
    revs = list(item.revisions())
    last_edit_timestamp = revs[0]['timestamp']
    delta = datetime.now() - last_edit_timestamp
    if delta.days < 3:
        return False
    for rev in revs:
        users.add(rev['user'])
    hoomans = get_hoomans(users)
    if len(hoomans) != 1:
        return False
    
    return hoomans[0]