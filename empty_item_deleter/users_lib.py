import requests
import json
def get_hoomans(users):
    try:
        with open('users_cache.json', 'r') as f:
            users_lib = json.loads(f.read())
    except:
        users_lib = {}
        with open('users_cache.json', 'w') as f:
            f.write(json.dumps(users_lib))

    hoomans = []
    for user in users:
        if user in users_lib:
            if users_lib[user] != 'bot':
                hoomans.append(user)
            continue
        params = {
            'action': 'query',
            'list': 'users',
            'format': 'json',
            'usprop': 'groups',
            'ususers': user
        }
        res = requests.get('https://www.wikidata.org/w/api.php', params=params).json()
        try:
            groups = res['query']['users'][0]['groups']
        except:
            hoomans.append(user)
            continue
        users_lib[user] = 'bot' if 'bot' in groups else 'hooman'
        with open('users_cache.json', 'w') as f:
            f.write(json.dumps(users_lib))
        if users_lib[user] != 'bot':
            hoomans.append(user)
    return hoomans